import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {routerTransition} from '../router.animations';
import {AuthService} from '../shared/services/auth.service';
import {NgForm} from '@angular/forms';
import {User} from '../user';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    providers: [AuthService],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
    private userResult: User;
    private userError;
    private isLoginError: boolean;

    constructor(public router: Router, public authService: AuthService) {
        this.isLoginError = false;
    }

    ngOnInit() {
        this.userResult = new User;
    }

    onLoggedin(form: NgForm) {
        localStorage.removeItem('api_token');
        localStorage.removeItem('isLoggedin');

        this.authService.login(form.value.email, form.value.password)
            .subscribe(
                (result) => {
                    this.userResult = new User;
                    this.userResult = result.data;
                    localStorage.setItem('api_token', this.userResult.api_token);
                    localStorage.setItem('name', this.userResult.name);
                    localStorage.setItem('email', this.userResult.email);
                    localStorage.setItem('isLoggedin', 'true');
                    this.router.navigate(['/dashboard']);
                    this.isLoginError = false;
                },
                (error) => {
                    this.isLoginError = true;
                    localStorage.removeItem('api_token');
                    localStorage.removeItem('name');
                    localStorage.removeItem('email');
                    localStorage.removeItem('isLoggedin');
                    this.router.navigate(['/login']);
                }
            );
    }

    onSignup() {
        this.router.navigate(['/signup']);
    }
}
