import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {SignupRoutingModule} from './signup-routing.module';
import {SignupComponent} from './signup.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {AuthService} from '../shared/services/auth.service';

@NgModule({
    imports: [
        CommonModule,
        SignupRoutingModule,
        FormsModule,
        HttpClientModule
    ],
    declarations: [SignupComponent],
    providers: [AuthService]
})
export class SignupModule {
}
