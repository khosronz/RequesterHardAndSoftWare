import {Component, OnInit} from '@angular/core';
import {routerTransition} from '../router.animations';
import {Router} from '@angular/router';
import {User} from '../user';
import {AuthService} from '../shared/services/auth.service';
import {NgForm} from '@angular/forms';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss'],
    providers: [AuthService],
    animations: [routerTransition()]
})
export class SignupComponent implements OnInit {
    private userResult: User;

    private isRegisterError: boolean;

    constructor(public router: Router, private authService: AuthService) {
        this.isRegisterError = false;
    }

    ngOnInit() {
    }

    onSignup(form: NgForm) {

        localStorage.removeItem('api_token');
        localStorage.removeItem('isLoggedin');
        // console.log(form.value.name, form.value.email, form.value.password, form.value.confirm);
        this.authService.signup(form.value.name, form.value.email, form.value.password, form.value.confirm)
            .subscribe(
                (result) => {
                    this.userResult = new User;
                    this.userResult = result.data;

                    localStorage.setItem('api_token', this.userResult.api_token);
                    localStorage.setItem('name', this.userResult.name);
                    localStorage.setItem('email', this.userResult.email);
                    localStorage.setItem('isLoggedin', 'true');
                    this.router.navigate(['/dashboard']);
                    this.isRegisterError = false;
                },
                (error) => {
                    this.isRegisterError = true;
                    localStorage.removeItem('api_token');
                    localStorage.removeItem('name');
                    localStorage.removeItem('email');
                    localStorage.removeItem('isLoggedin');
                    this.router.navigate(['/signup']);
                }
            );

    }

    onLogin() {
        this.router.navigate(['/login']);
    }
}
