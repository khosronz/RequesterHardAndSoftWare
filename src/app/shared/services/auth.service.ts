import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import 'rxjs/Rx';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';

@Injectable()
export class AuthService {
    private apiUrl: string;
    private apiAuthUrl: string;
    private httpHeaders: HttpHeaders;

    constructor(private _http: HttpClient) {
        this.apiUrl = 'http://localhost:8000/api/v1/';
        this.apiAuthUrl = '';
        this.httpHeaders = new HttpHeaders;
        this.httpHeaders.set('access', 'application/json');
    }

    signup(name: string, email: string, password: string, confirm: string): any {
        // It's Body
        const user = {
            'name': name,
            'email': email,
            'password': password,
            'confirm': confirm
        };
        const signupUrl = this.apiUrl + 'signup';
        return this._http.post(signupUrl, user, {headers: this.httpHeaders})
            .map((result) => {
                return result;
            });
    }

    login(email: string, password: string): any {
        // It's Body
        const user = {
            'email': email,
            'password': password
        };

        const loginUrl = this.apiUrl + 'login';
        return this._http.post(loginUrl, user, {headers: this.httpHeaders})
            .map((result) => {
                return result;
            });
    }


}
