import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import 'rxjs/Rx';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';

@Injectable()
export class UserService {
    private apiUrl: string;
    private apiAuthUrl: string;
    private httpHeaders: HttpHeaders;

    constructor(private _http: HttpClient) {
        this.apiUrl = 'http://localhost:8000/api/v1/';
        this.apiAuthUrl = '';
        this.httpHeaders = new HttpHeaders;
        this.httpHeaders.set('access', 'application/json');
    }

    getAllUsers(): any {
        const usersUrl = this.apiUrl + 'users';
        return this._http.get(usersUrl, {headers: this.httpHeaders})
            .map((result) => {
                return result;
            });
    }

}
