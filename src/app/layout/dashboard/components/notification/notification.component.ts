import { Component, OnInit } from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';

@Component({
    selector: 'app-notification',
    templateUrl: './notification.component.html',
    styleUrls: ['./notification.component.scss'],
    providers: [TranslateModule]
})
export class NotificationComponent implements OnInit {
    constructor() { }
    ngOnInit() { }
}
