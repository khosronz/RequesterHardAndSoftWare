import { Component, OnInit } from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';

@Component({
    selector: 'app-chat',
    templateUrl: './chat.component.html',
    styleUrls: ['./chat.component.scss'],
    providers: [TranslateModule]
})
export class ChatComponent implements OnInit {
    constructor() { }
    ngOnInit() { }
}
