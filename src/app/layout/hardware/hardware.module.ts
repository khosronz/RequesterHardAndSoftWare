import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {HardwareRoutingModule} from './hardware-routing.module';
import {HardwareComponent} from './hardware.component';
import {TranslateModule} from '@ngx-translate/core';
import {PageHeaderModule} from '../../shared/modules/page-header/page-header.module';
import {FormsModule} from '@angular/forms';
import {NgbTabsetModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
    imports: [
        CommonModule,
        NgbTabsetModule.forRoot(),
        HardwareRoutingModule,
        TranslateModule,
        PageHeaderModule,
        FormsModule
    ],
    declarations: [HardwareComponent]
})
export class HardwareModule {
}
