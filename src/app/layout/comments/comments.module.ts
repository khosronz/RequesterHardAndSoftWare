import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {CommentsRoutingModule} from './comments-routing.module';
import {CommentsComponent} from './comments/comments.component';
import {FormsModule} from '@angular/forms';
import {NgbTabsetModule} from '@ng-bootstrap/ng-bootstrap';
import {TranslateModule} from '@ngx-translate/core';
import {PageHeaderModule} from '../../shared/modules/page-header/page-header.module';

@NgModule({
    imports: [
        CommonModule,
        CommentsRoutingModule,
        FormsModule,
        NgbTabsetModule.forRoot(),
        TranslateModule,
        PageHeaderModule
    ],
    declarations: [CommentsComponent]
})
export class CommentsModule {
}
