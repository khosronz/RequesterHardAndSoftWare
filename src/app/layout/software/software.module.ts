import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {NgbTabsetModule} from '@ng-bootstrap/ng-bootstrap';

import {SoftwareRoutingModule} from './software-routing.module';
import {SoftwareComponent} from './software.component';

import {FormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';
import {PageHeaderModule} from '../../shared/modules/page-header/page-header.module';


@NgModule({
    imports: [
        CommonModule,
        NgbTabsetModule.forRoot(),
        SoftwareRoutingModule,
        FormsModule,
        TranslateModule,
        PageHeaderModule
    ],
    declarations: [SoftwareComponent]
})
export class SoftwareModule {
}
