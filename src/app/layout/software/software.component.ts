import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {HttpClient} from '@angular/common/http';

@Component({
    selector: 'app-software',
    templateUrl: './software.component.html',
    styleUrls: ['./software.component.scss']
})
export class SoftwareComponent implements OnInit {

    constructor(private _http: HttpClient) {

    }


    ngOnInit() {
    }

    onSubmit(form: NgForm) {
        console.log(form.value);
        console.log(form.valid);
    }

    onSubmitSearch(form: NgForm) {
        console.log(form.value);
        console.log(form.valid);
    }

}
