import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PostsRoutingModule} from './posts-routing.module';
import {PostsComponent} from './posts.component';
import {NgbTabsetModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';
import {PageHeaderModule} from '../../shared/modules/page-header/page-header.module';
import {FroalaEditorModule, FroalaViewModule} from 'angular-froala-wysiwyg';

@NgModule({
    imports: [
        CommonModule,
        PostsRoutingModule,
        NgbTabsetModule.forRoot(),
        FormsModule,
        TranslateModule,
        PageHeaderModule,
        FroalaEditorModule.forRoot(),
        FroalaViewModule.forRoot()
    ],
    declarations: [PostsComponent]
})
export class PostsModule {
}
