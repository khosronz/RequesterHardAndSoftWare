import {Component, OnInit} from '@angular/core';
import {UserService} from '../../shared/services/user.service';
import {User} from '../../user';

@Component({
    selector: 'app-users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.scss'],
    providers: [UserService]
})
export class UsersComponent implements OnInit {
    private userResult = [];
    private userError;

    constructor(private userService: UserService) {
    }

    ngOnInit() {
    }

    getAllUsers() {
        this.userService.getAllUsers()
            .subscribe(
                (result) => {
                    this.userResult = result.data;
                }
            );
    }

}
