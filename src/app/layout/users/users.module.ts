import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {UsersRoutingModule} from './users-routing.module';
import {UsersComponent} from './users.component';
import {FormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';
import {PageHeaderModule} from '../../shared/modules/page-header/page-header.module';

@NgModule({
    imports: [
        CommonModule,
        UsersRoutingModule,
        FormsModule,
        TranslateModule,
        PageHeaderModule
    ],
    declarations: [UsersComponent]
})
export class UsersModule {
}
